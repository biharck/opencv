package br.master.opencv;

import java.util.Date;

import org.opencv.core.Mat;

public class WoundAnalysis {

	private WoundAnalysisType woundAnalysisType;
	private double woundLength;
	private double necrosisPercentual;
	private double granulationPercentual;
	private Date analysisDate;
	private Mat analyzedImage;
	private String analyzedImageName;
	private Professional professional;
	
	public WoundAnalysisType getWoundAnalysisType() {
		return woundAnalysisType;
	}
	public double getWoundLength() {
		return woundLength;
	}
	public double getNecrosisPercentual() {
		return necrosisPercentual;
	}
	public double getGranulationPercentual() {
		return granulationPercentual;
	}
	public Date getAnalysisDate() {
		return analysisDate;
	}
	public Mat getAnalyzedImage() {
		return analyzedImage;
	}
	public String getAnalyzedImageName() {
		return analyzedImageName;
	}
	public Professional getProfessional() {
		return professional;
	}
	public void setWoundAnalysisType(WoundAnalysisType woundAnalysisType) {
		this.woundAnalysisType = woundAnalysisType;
	}
	public void setWoundLength(double woundLength) {
		this.woundLength = woundLength;
	}
	public void setNecrosisPercentual(double necrosisPercentual) {
		this.necrosisPercentual = necrosisPercentual;
	}
	public void setGranulationPercentual(double granulationPercentual) {
		this.granulationPercentual = granulationPercentual;
	}
	public void setAnalysisDate(Date analysisDate) {
		this.analysisDate = analysisDate;
	}
	public void setAnalyzedImage(Mat analyzedImage) {
		this.analyzedImage = analyzedImage;
	}
	public void setAnalyzedImageName(String analyzedImageName) {
		this.analyzedImageName = analyzedImageName;
	}
	public void setProfessional(Professional professional) {
		this.professional = professional;
	}
}
