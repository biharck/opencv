package br.master.opencv;

public class WoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	WoundException(){}
	
	WoundException(String message){
		super(message);
	}
	
	WoundException(Throwable throwable){
		super(throwable);
	}
	
	WoundException(String message, Throwable throwable){
		super(message, throwable);
	}
	
}
