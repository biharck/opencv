package br.master.opencv;

public enum WoundAnalysisType {

	MANUAL(1, "Professional Analysis"),
	AUTOMATIC(2, "Automatic Analysis");
	
	private WoundAnalysisType(int type, String description){
		this.type = type;
		this.description = description;
	}
	
	private int type;
	private String description;
	
	public int getType() {
		return type;
	}
	public String getDescription() {
		return description;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return getDescription();
	}
}
