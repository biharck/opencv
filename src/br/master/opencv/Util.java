package br.master.opencv;

public class Util {

	public static String PATH = "";
	
	
	public static String getFullSourceImagePath(Wound wound){
		return PATH+wound.getName()+wound.getExtension();
	}
	
	public static String getFullTargetImagePath(Wound wound, String complement){
		if(complement!=null)
			return PATH+wound.getName()+"_"+complement+"_Restult"+wound.getExtension();
		else
			return PATH+wound.getName()+"_Restult"+wound.getExtension();
	}
	
	public static int getLastPosition(ROI roi){
		return roi.getPoints().get(roi.getPoints().size()-1);
	}
	
//	public static boolean isGreen(double r, double g, double b){
//		r = (int) r;
//		g = (int) g;
//		b = (int) b;
//		
//		if(r == 0d && g == 255d && b == 0d)
//			return true;
//		else if((r >= 0d && r <= 140d) && (g >=170d && g <= 255d) && (b >= 0d && b <= 140d))
//			return true;
//		else if((r >=0d && r <= 150d) && (g >=166d && g <= 250d) && (b >=78d && b <= 190d))
//			return true;
//		else 
//			return false;
//	}
	
public static boolean isGreen(double r, double g, double b){

		int[] hsv = new int[3];
		rgb2hsl((int)r, (int)g, (int)b, hsv);

		float h = hsv[0];
		float s = hsv[1];
		float v = hsv[2];
		
//algumas feridas precisam de alterar o valor mínimo de h para 68
		
		if(h >= 85 && h <= 150)
			if(s >= 20)
				if(v >= 20 && v <= 75)
					return true;

		return false;
	}
	
	public static int[] getHSV(int red, int green, int blue) {
        int[] Results = new int[3];
        int HSV_H = 0;
        int HSV_S = 0;
        int HSV_V = 0;

        double MaxHSV = (Math.max(red, Math.max(green, blue)));
        double MinHSV = (Math.min(red, Math.min(green, blue)));

        HSV_V = (int) (MaxHSV);

        HSV_S = 0;
        if (MaxHSV != 0) HSV_S = (int) (255 - 255 * (MinHSV / MaxHSV));

        if (MaxHSV != MinHSV) {

            int IntegerMaxHSV = (int) (MaxHSV);

            if (IntegerMaxHSV == red && green >= blue) {
                HSV_H = (int) (60 * (green - blue) / (MaxHSV - MinHSV));
            } else if (IntegerMaxHSV == red && green < blue) {
                HSV_H = (int) (359 + 60 * (green - blue) / (MaxHSV - MinHSV));
            } else if (IntegerMaxHSV == green) {
                HSV_H = (int) (119 + 60 * (blue - red) / (MaxHSV - MinHSV));
            } else if (IntegerMaxHSV == blue) {
                HSV_H = (int) (239 + 60 * (red - green) / (MaxHSV - MinHSV));
            }


        } else HSV_H = 0;

        Results[0] = HSV_H;
        Results[1] = HSV_S;
        Results[2] = HSV_V;

        return (Results);
    }
	
	public static void rgb2hsl(int r, int g, int b, int hsl[]) {

		

		float var_R = ( r / 255f );                    

		float var_G = ( g / 255f );

		float var_B = ( b / 255f );

		

		float var_Min;    //Min. value of RGB

		float var_Max;    //Max. value of RGB

		float del_Max;    //Delta RGB value

						 

		if (var_R > var_G) 

			{ var_Min = var_G; var_Max = var_R; }

		else 

			{ var_Min = var_R; var_Max = var_G; }



		if (var_B > var_Max) var_Max = var_B;

		if (var_B < var_Min) var_Min = var_B;



		del_Max = var_Max - var_Min; 

								 

		float H = 0, S, L;

		L = ( var_Max + var_Min ) / 2f;

	

		if ( del_Max == 0 ) { H = 0; S = 0; } // gray

		else {                                //Chroma

			if ( L < 0.5 ) 

				S = del_Max / ( var_Max + var_Min );

			else           

				S = del_Max / ( 2 - var_Max - var_Min );

	

			float del_R = ( ( ( var_Max - var_R ) / 6f ) + ( del_Max / 2f ) ) / del_Max;

			float del_G = ( ( ( var_Max - var_G ) / 6f ) + ( del_Max / 2f ) ) / del_Max;

			float del_B = ( ( ( var_Max - var_B ) / 6f ) + ( del_Max / 2f ) ) / del_Max;

	

			if ( var_R == var_Max ) 

				H = del_B - del_G;

			else if ( var_G == var_Max ) 

				H = ( 1 / 3f ) + del_R - del_B;

			else if ( var_B == var_Max ) 

				H = ( 2 / 3f ) + del_G - del_R;

			if ( H < 0 ) H += 1;

			if ( H > 1 ) H -= 1;

		}

		hsl[0] = (int) Math.round(360*H);

		hsl[1] = (int) Math.round(S*100);

		hsl[2] = (int) Math.round(L*100);

	}
}
