package br.master.opencv;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

public class WoundProcessor {

	public static List<ROI> addROI(List<ROI> rois, int row, int posicao) {
		boolean marcador = false;

		for (ROI roi : rois) {
			if (roi.getRow() == row) {
				marcador = true;
				roi.setEnd(posicao);

				if ((posicao - Util.getLastPosition(roi)) > 20) {
					roi.getPoints().add(posicao);
				}
				break;
			}
		}
		if (!marcador || rois.isEmpty()) {
			ROI r = new ROI(row, posicao);
			r.setEnd(posicao);
			List<Integer> points = new ArrayList<Integer>();
			points.add(posicao);
			r.setPoints(points);
			rois.add(r);
		}
		return rois;
	}

	/**
	 * Blue = 0 Green = 1 Red = 2
	 * 
	 * @param wound
	 * @param subImage
	 * @param listaRoi
	 * @return
	 */
	public List<ROI> getWoundDelimitation(Wound wound, Mat subImage,
			List<ROI> listaRoi) {

		for (int i = 0; i < wound.getOriginalImage().rows(); i++) {
			for (int j = 0; j < wound.getOriginalImage().cols(); j++) {
				double rgb[] = wound.getOriginalImage().get(i, j);
				// if((rgb[2]==0d && rgb[1]==255d && rgb[0]==0d)){
				if (Util.isGreen(rgb[2], rgb[1], rgb[0])) {
					subImage.put(i, j, 255);
					listaRoi = addROI(listaRoi, i, j);
				}
			}
		}

		return listaRoi;
	}

	public static int getReferenceArea(Wound wound) {

		int qtdPixels = 0;

		for (int i = 0; i < wound.getOriginalImage().rows(); i++) {
			for (int j = 0; j < wound.getOriginalImage().cols(); j++) {
				double rgb[] = wound.getOriginalImage().get(i, j);
				if ((rgb[2] == 0d && rgb[1] == 0d && rgb[0] == 255d)) {
					// if(Util.isGreen(rgb[2],rgb[1],rgb[0])){
					qtdPixels++;
				}
			}
		}
		return qtdPixels;
	}

	public Mat readImage(Wound wound) throws WoundException {

		if (wound.getName() == null || wound.getName().isEmpty())
			throw new WoundException("Wound name is required");

		if (wound.getExtension() == null || wound.getExtension().isEmpty())
			throw new WoundException("Wound extension is required");

		File f = new File(Util.getFullSourceImagePath(wound));
		return Highgui.imread(f.getAbsolutePath());
	}

	public boolean writeImage(Wound wound, String complement, Mat image) {
		try {
			Highgui.imwrite(Util.getFullTargetImagePath(wound, complement),
					image);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Enviar lista sem circular a regiao.... remover por fora...
	 * 
	 * @param listaRoi
	 * @param subImage
	 * @return
	 */
	public static ResultInspect findNecrosis(List<ROI> listaRoi, Mat subImage) {

		Mat temp = subImage.clone();
		int sizeTemp = 0;
		boolean isNecrosisCheckedBefore = false;

		for (ROI roi : listaRoi) {
			for (int i = roi.getBegin(); i < roi.getEnd(); i++) {
				subImage.put(roi.getRow(), i, 255, 255, 255);
				sizeTemp++;
			}
		}

		int necrose = 0;
		int esfacelo = 0;

		for (ROI roi : listaRoi) {
			for (int i = roi.getBegin(); i < roi.getEnd(); i++) {
				
				isNecrosisCheckedBefore = false;
				double[] bgr = temp.get(roi.getRow(), i);

				int r = (int) bgr[2];
				int g = (int) bgr[1];
				int b = (int) bgr[0];

				int[] hsv = new int[3];
				Util.rgb2hsl(r, g, b, hsv);

				float h = hsv[0];
				float s = hsv[1];
				float v = hsv[2];

				// GRANULAÇÃO: estou colorindo tudo antes
				subImage.put(roi.getRow(), i, 0, 0, 255);// vermelho

				// REGRA 1: NECROSE PRETA Somnte necrose em que o v esteja
				// abaixo de 18 independente dos outros valores =
				if (v <= 18) {
					necrose++;
					subImage.put(roi.getRow(), i, 121, 57, 12);// azul,lembre-se Blue Red Green
					isNecrosisCheckedBefore = true;
				}

				// REGRA 2: NECROSE AMARELA definindo o amarelo da necrose
				else if (h >= 50 && h <= 60) {
					if (s >= 20) {
						if (v <= 60) {
							// esta linha debaixo soma os pixels de necrose
							necrose++;
							// esta linha de baixo, colore a ferida
							subImage.put(roi.getRow(), i, 121, 57, 12);// azul,lembre-se Blue Red Green
							isNecrosisCheckedBefore = true;
						}
					}
				}

				// REGRA 3: necrose amarela
				else if (h >= 25 && h <= 50) {
					necrose++;
					subImage.put(roi.getRow(), i, 121, 57, 12);// azul,lembre-se Blue Red Green
					isNecrosisCheckedBefore = true;
				}

				// REGRA 4: NECROSE MARROM
//				else if (h >= 19 && h <= 60) {
//					if (s >= 19) {
//						if (v >= 19 && v <= 60) {
//							// esta linha debaixo soma os pixels de necrose
//							necrose++;
//							// esta linha de baixo, colore a ferida
//							subImage.put(roi.getRow(), i, 121, 57, 12);// azul,lembre-se Blue Red Green
//							isNecrosisCheckedBefore = true;
//						}
//					}
//				}

				
				else if (h >= 30 && h <= 49) {
					if (s > 19) {
						if (v <= 35) {
							// esta linha debaixo soma os pixels de necrose
							necrose++;
							// esta linha de baixo, colore a ferida
							subImage.put(roi.getRow(), i, 121, 57, 12);// azul,lembre-se Blue Red Green
							isNecrosisCheckedBefore = true;
						}
					}
				}
					
				else if (h <= 25) {
					if (s >= 19 && s <= 39) {
						if (v <= 25) {
							// esta linha debaixo soma os pixels de necrose
							necrose++;
							// esta linha de baixo, colore a ferida
							subImage.put(roi.getRow(), i, 121, 57, 12);// azul,lembre-se Blue Red Green
							isNecrosisCheckedBefore = true;
						}
					}
				}
				
				// REGRA 5: NECROSE VERDE
				
				else if (h >= 50 && s <= 84) {
					if (v <= 50) {
						// esta linha debaixo soma os pixels de necrose
						necrose++;
						// esta linha de baixo, colore a ferida
						subImage.put(roi.getRow(), i, 121, 57, 12);// azul,lembre-se Blue Red Green
						isNecrosisCheckedBefore = true;
						}
				}
				
				//##########################################
				//##### ESFACELO COMECA A PARTIR DAQUI ......

				// REGRA 6: ESFACELO
				if (h >= 30 && h <= 49) {
					if (s >= 20) {
						if (v >= 35 && v <= 65) {
							// esta linha debaixo soma os pixels de esfacelo
							esfacelo++;
							// esta linha debaixo, colore a ferida
							subImage.put(roi.getRow(), i, 0, 255, 255);// azul,lembre-se Blue Red Green
							if(isNecrosisCheckedBefore){
								necrose--;
							}
						}
					}
				}

				else if (h >= 50 && h <= 60) {
					if (s >= 70) {
						if (v >= 51 && v <= 90) {
							// esta linha debaixo soma os pixels de esfacelo
							esfacelo++;
							// esta linha debaixo, colore a ferida
							subImage.put(roi.getRow(), i, 0, 255, 255);// azul,lembre-se Blue Red Green
							if(isNecrosisCheckedBefore){
								necrose--;
							}
						}
					}
				}

				else if (h >= 61 && h <= 70) {
					if (s >= 40) {
						if (v >= 50 && v <= 80) {
							// esta linha debaixo soma os pixels de esfacelo
							esfacelo++;
							// esta linha debaixo, colore a ferida
							subImage.put(roi.getRow(), i, 0, 255, 255);// azul,lembre-se Blue Red Green
							if(isNecrosisCheckedBefore){
								necrose--;
							}
						}
					}
				}
			}
		}

		ResultInspect resultInspect = new ResultInspect();
		resultInspect.setMat(subImage);
		int[] dados = new int[4];
		dados[0] = sizeTemp;
		dados[1] = necrose;
		dados[2] = sizeTemp - (necrose + esfacelo);
		dados[3] = esfacelo;

		resultInspect.setValues(dados);

		return resultInspect;
	}

	public static boolean isBoundary(Wound wound, int x, int y) {

		boolean up = false;
		boolean down = false;
		boolean right = false;
		boolean left = false;
		int qtdVezesVistaUp = 0;
		int qtdVezesVistaDown = 0;
		int qtdVezesVistaLeft = 0;
		int qtdVezesVistaRight = 0;

		for (int i = 0; i < wound.getOriginalImage().rows(); i++) {
			qtdVezesVistaLeft = 0;
			qtdVezesVistaRight = 0;
			qtdVezesVistaUp = 0;
			qtdVezesVistaDown = 0;
			for (int j = 0; j < wound.getOriginalImage().cols(); j++) {

				double rgb[] = wound.getOriginalImage().get(i, j);

				if (j == 15 && i == 33)
					System.out.println();

				// if(x==i && (rgb[2]==0d && rgb[1]==255d && rgb[0]==0d) && j >
				// y){
				if (x == i && (Util.isGreen(rgb[2], rgb[1], rgb[0])) && j > y) {
					qtdVezesVistaRight++;
					right = true;
				}
				// if(x==i && (rgb[2]==0d && rgb[1]==255d && rgb[0]==0d) && j <
				// y){
				if (x == i && (Util.isGreen(rgb[2], rgb[1], rgb[0])) && j < y) {
					qtdVezesVistaLeft++;
					left = true;
				}
				// if(y==j && (rgb[2]==0d && rgb[1]==255d && rgb[0]==0d) && i <
				// x){
				if (y == j && (Util.isGreen(rgb[2], rgb[1], rgb[0])) && i < x) {
					qtdVezesVistaUp++;
					up = true;
				}
				// if(y==j && (rgb[2]==0d && rgb[1]==255d && rgb[0]==0d) && i >
				// x){
				if (y == j && (Util.isGreen(rgb[2], rgb[1], rgb[0])) && i > x) {
					qtdVezesVistaDown++;
					down = true;
				}
			}
		}

		if ((qtdVezesVistaDown + qtdVezesVistaUp + qtdVezesVistaRight + qtdVezesVistaLeft) > 4)
			return false;

		return right && left && up && down;
	}

	/**
	 * Metodo responsavel por desenhar a nova imagem apenas dentro da
	 * delimitacao Consiste em realizar um loop dentro dos pontos de interesse
	 * encontrados em seguida preencher a linha da imagem realizando um novo
	 * loop deste o primeiro ponto encontrado ate o ultimo
	 * 
	 * @param wound
	 * @param listaRoi
	 * @param subImage
	 * @return
	 */
	public static Mat getImageByROI(Wound wound, List<ROI> listaRoi,
			Mat subImage) {

		for (ROI roi : listaRoi) {
			for (int i = roi.getBegin(); i < roi.getEnd(); i++) {
				double rgb[] = wound.getOriginalImage().get(roi.getRow(), i);
				// if((rgb[2]<10d && rgb[1]>240d && rgb[0]<10d))
				// if((rgb[2]==0d && rgb[1]==255d && rgb[0]==0d))
				if (Util.isGreen(rgb[2], rgb[1], rgb[0]))
					continue;

				// if(isBoundary(wound, roi.getRow(), i)){
				// System.out.println("linha"+roi.getRow());
				// System.out.println("coluna"+i);
				// if(roi.getRow() == 22 && i == 33)
				// subImage.put(roi.getRow(), i, new double[]{0,255,0});
				// else
				subImage.put(roi.getRow(), i,
						wound.getOriginalImage().get(roi.getRow(), i));
				// }
			}
		}
		return subImage;
	}

}
