package br.master.opencv;

public class Professional {
	
	private String name;
	private String hospital;
	private String jobTitle;
	private int YearsInTheProfession;
	
	public String getName() {
		return name;
	}
	public String getHospital() {
		return hospital;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public int getYearsInTheProfession() {
		return YearsInTheProfession;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public void setYearsInTheProfession(int yearsInTheProfession) {
		YearsInTheProfession = yearsInTheProfession;
	}
}
