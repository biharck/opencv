package br.master.opencv;

import org.opencv.core.Mat;

public class Wound {
	
	private String name;
	private String extension;
	private String description;
	private Mat originalImage;
	private WoundAnalysis woundAnalysis;
	
	public String getName() {
		return name;
	}
	public String getExtension() {
		return extension;
	}
	public String getDescription() {
		return description;
	}
	public Mat getOriginalImage() {
		return originalImage;
	}
	public WoundAnalysis getWoundAnalysis() {
		return woundAnalysis;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setOriginalImage(Mat originalImage) {
		this.originalImage = originalImage;
	}
	public void setWoundAnalysis(WoundAnalysis woundAnalysis) {
		this.woundAnalysis = woundAnalysis;
	}
}
