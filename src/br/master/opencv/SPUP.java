package br.master.opencv;

public class SPUP {

	
	private String originalImage;
	private String delimitedImage;
	private String cutImage;
	private String resultImage;
	private String area;
	private String necrose;
	private String esfacelo;
	private String granulacao;
	private String grafh;
	
	public String getOriginalImage() {
		return originalImage;
	}
	public String getDelimitedImage() {
		return delimitedImage;
	}
	public String getCutImage() {
		return cutImage;
	}
	public String getResultImage() {
		return resultImage;
	}
	public String getArea() {
		return area;
	}
	public String getNecrose() {
		return necrose;
	}
	public String getEsfacelo() {
		return esfacelo;
	}
	public String getGranulacao() {
		return granulacao;
	}
	public String getGrafh() {
		return grafh;
	}
	
	public void setOriginalImage(String originalImage) {
		this.originalImage = originalImage;
	}
	public void setDelimitedImage(String delimitedImage) {
		this.delimitedImage = delimitedImage;
	}
	public void setCutImage(String cutImage) {
		this.cutImage = cutImage;
	}
	public void setResultImage(String resultImage) {
		this.resultImage = resultImage;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public void setNecrose(String necrose) {
		this.necrose = necrose;
	}
	public void setEsfacelo(String esfacelo) {
		this.esfacelo = esfacelo;
	}
	public void setGranulacao(String granulacao) {
		this.granulacao = granulacao;
	}
	public void setGrafh(String grafh) {
		this.grafh = grafh;
	}
	
}
