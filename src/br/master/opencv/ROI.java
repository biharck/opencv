package br.master.opencv;

import java.util.List;

public class ROI {
	
	private int row;
	private int begin;
	private int end;
	private List<Integer> points;
	
	public ROI(){
		
	}
	public ROI(int row, int begin){
		this.row = row;
		this.begin = begin;
	}
	
	public int getRow() {
		return row;
	}
	public int getBegin() {
		return begin;
	}
	public int getEnd() {
		return end;
	}
	public List<Integer> getPoints() {
		return points;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public void setBegin(int begin) {
		this.begin = begin;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	public void setPoints(List<Integer> points) {
		this.points = points;
	}
}
