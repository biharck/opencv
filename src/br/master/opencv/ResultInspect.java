package br.master.opencv;

import org.opencv.core.Mat;

public class ResultInspect {
	
	private Mat mat;
	private int[] values;
	public Mat getMat() {
		return mat;
	}
	public int[] getValues() {
		return values;
	}
	public void setMat(Mat mat) {
		this.mat = mat;
	}
	public void setValues(int[] values) {
		this.values = values;
	}
	
	

}
