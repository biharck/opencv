package br.master.opencv.test;


import java.awt.Color;
import java.io.File;
import java.math.BigDecimal;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

public class PieChart {

	
	public static void main(String[] args) {
		PieChart p = new PieChart();
		try {
			p.getPieChart(new BigDecimal(2), new BigDecimal(2), new BigDecimal(2), "/Applications/Developer/JAVA/workspace/SPUP/");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public File getPieChart(BigDecimal granulacao, BigDecimal necrose, BigDecimal esfacelo, String path) throws Exception {

		Color[] colors = { new Color(255, 0, 0), Color.BLUE, Color.YELLOW };
		DefaultPieDataset dataset = new DefaultPieDataset();
		dataset.setValue("Granula��o", new Double(granulacao.doubleValue()));
		dataset.setValue("Necrose", new Double(necrose.doubleValue()));
		dataset.setValue("Esfacelo", new Double(esfacelo.doubleValue()));

		JFreeChart chart = ChartFactory.createPieChart3D("An�lise da Ferida", // chart
																				// title
				dataset, // data
				true, // include legend
				true, false);

		
		final PiePlot plot = (PiePlot) chart.getPlot();
		plot.setBackgroundPaint(Color.WHITE);
		PieRenderer renderer = new PieRenderer(colors);
		renderer.setColor(plot, dataset);
		//plot.setStartAngle(270);
//		plot.setForegroundAlpha(0.60f);
		//plot.setInteriorGap(0.02);
		int width = 320; /* Width of the image */
		int height = 250; /* Height of the image */
		File pieChart3D = new File(path+"pie_Chart3D.jpeg");
		ChartUtilities.saveChartAsJPEG(pieChart3D, chart, width, height);
		return pieChart3D;
	}

	static class PieRenderer {
		private Color[] color;

		public PieRenderer(Color[] color) {
			this.color = color;
		}

		public void setColor(PiePlot plot, DefaultPieDataset dataset) {
			List<Comparable> keys = dataset.getKeys();
			int aInt;

			for (int i = 0; i < keys.size(); i++) {
				aInt = i % this.color.length;
				plot.setSectionPaint(keys.get(i), this.color[aInt]);
			}
		}
	}

}
