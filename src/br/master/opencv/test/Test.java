package br.master.opencv.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import br.master.opencv.ROI;
import br.master.opencv.ResultInspect;
import br.master.opencv.SPUP;
import br.master.opencv.Util;
import br.master.opencv.Wound;
import br.master.opencv.WoundException;
import br.master.opencv.WoundProcessor;

public class Test {
	
	
	public static void main(String[] args) {
		
		String image = "SC_M.V.F._TIBIAL_D_1.png";
		
		Test t = new Test();
		SPUP s = t.execute("/Users/biharck/wound/piloto/", image);
		
//		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
//		
//		WoundProcessor processor = new WoundProcessor();
//		
//		Wound wound = new Wound();
//		try {
//			wound.setName("SC_M.V.F._TIBIAL_D_1");
//			wound.setExtension(".png");
//			wound.setOriginalImage(processor.readImage(wound));
//		} catch (WoundException e) {
//			e.printStackTrace();
//		}
//		
//		Mat subImage = Mat.zeros(wound.getOriginalImage().size(), CvType.CV_8UC1);
//		
//		List<ROI> listaRoi = new ArrayList<ROI>();
//		listaRoi = processor.getWoundDelimitation(wound, subImage, listaRoi);
//		
//		Mat subImage2 = Mat.zeros(wound.getOriginalImage().size(), CvType.CV_8UC3);
//		
//		subImage2 = WoundProcessor.getImageByROI(wound, listaRoi, subImage2);
//		
//		processor.writeImage(wound, "1", subImage);
//		processor.writeImage(wound, "2", subImage2);
//		
//		Mat subImage3 = Mat.zeros(subImage2.size(), CvType.CV_8UC3);
//		ResultInspect resultInspect = WoundProcessor.findNecrosis(listaRoi, subImage2);
//		subImage3 = resultInspect.getMat();
//		
//		processor.writeImage(wound, "3", subImage3);
//		System.out.println("---- dados computacionais ----");
//		System.out.println("Total Pixel: " + resultInspect.getValues()[0]);
//		System.out.println("Necrose: "+resultInspect.getValues()[1]);
//		System.out.println("Granulação: "+resultInspect.getValues()[2]);
//		System.out.println("Esfacelo: "+resultInspect.getValues()[3]);
//		System.out.println("Total Pixel quadrado: " + processor.getReferenceArea(wound));
//		System.out.println("-------------------------------");
//		System.out.println();
//		System.out.println("-------- Resultado Humano ----- ");
//		System.out.println("Área em cm^2: " + new Double(resultInspect.getValues()[0])/ new Double(processor.getReferenceArea(wound)));
//		System.out.println("Área de Necrose: " + new Double(resultInspect.getValues()[1])/new Double(processor.getReferenceArea(wound)));
//		System.out.println("Área de Granulação: " + new Double(resultInspect.getValues()[2])/new Double(processor.getReferenceArea(wound)));
//		System.out.println("Área de Esfacelo: " + new Double(resultInspect.getValues()[3])/new Double(processor.getReferenceArea(wound)));
//		System.out.println("% de Necrose: " + (new Double(resultInspect.getValues()[1])/new Double(resultInspect.getValues()[0]))*100 );
//		System.out.println("% de Granulação: " + (new Double(resultInspect.getValues()[2])/new Double(resultInspect.getValues()[0]))*100 );
//		System.out.println("% de Esfacelo: " + (new Double(resultInspect.getValues()[3])/new Double(resultInspect.getValues()[0]))*100 );
//		System.out.println("---------------------------------");
//		
//		
//		try {
//			 
//			String content = "-------------------------------------------------";
//			content += "\nFerida: " + wound.getName();
//			content +=  "\nArea em cm^2: " + new Double(resultInspect.getValues()[0])/ new Double(processor.getReferenceArea(wound));
//			content += "\nArea de Necrose: " + new Double(resultInspect.getValues()[1])/new Double(processor.getReferenceArea(wound));
//			content += "\nArea de Granulacao: " + new Double(resultInspect.getValues()[2])/new Double(processor.getReferenceArea(wound));
//			content += "\nArea de Esfacelo: " + new Double(resultInspect.getValues()[3])/new Double(processor.getReferenceArea(wound));
//			content += "\n% de Necrose: " + (new Double(resultInspect.getValues()[1])/new Double(resultInspect.getValues()[0]))*100 ;
//			content += "\n% de Granulacao: " + (new Double(resultInspect.getValues()[2])/new Double(resultInspect.getValues()[0]))*100 ;
//			content += "\n% de Esfacelo: " + (new Double(resultInspect.getValues()[3])/new Double(resultInspect.getValues()[0]))*100 ;
//			content += "\n-------------------------------------------------";
//			
//			wound.setExtension(".txt");
//			File file = new File(Util.getFullTargetImagePath(wound, "ResultadoTexto"));
// 
//			if (!file.exists()) {
//				file.createNewFile();
//			}
// 
//			FileWriter fw = new FileWriter(file.getAbsoluteFile());
//			BufferedWriter bw = new BufferedWriter(fw);
//			bw.write(content);
//			bw.close();
// 
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
	}
	
	public SPUP execute(String path, String image) {
		
		Util.PATH = path;
		
		String[] split = image.split("\\.");
		String ext = split[split.length - 1];
		System.out.println(ext);
		System.out.println(image.replace("."+ext, ""));

		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		SPUP spup = new SPUP();
		
		WoundProcessor processor = new WoundProcessor();
		
		Wound wound = new Wound();
		try {
			wound.setName(image.replace("."+ext, ""));
			wound.setExtension("."+ext);
			wound.setOriginalImage(processor.readImage(wound));
		} catch (WoundException e) {
			e.printStackTrace();
		}
		
		Mat subImage = Mat.zeros(wound.getOriginalImage().size(), CvType.CV_8UC1);
		
		List<ROI> listaRoi = new ArrayList<ROI>();
		listaRoi = processor.getWoundDelimitation(wound, subImage, listaRoi);
		
		Mat subImage2 = Mat.zeros(wound.getOriginalImage().size(), CvType.CV_8UC3);
		
		subImage2 = WoundProcessor.getImageByROI(wound, listaRoi, subImage2);
		
		processor.writeImage(wound, "1", subImage);
		processor.writeImage(wound, "2", subImage2);
		
		Mat subImage3 = Mat.zeros(subImage2.size(), CvType.CV_8UC3);
		ResultInspect resultInspect = WoundProcessor.findNecrosis(listaRoi, subImage2);
		subImage3 = resultInspect.getMat();
		
		processor.writeImage(wound, "3", subImage3);
		System.out.println("---- dados computacionais ----");
		System.out.println("Total Pixel: " + resultInspect.getValues()[0]);
		System.out.println("Necrose: "+resultInspect.getValues()[1]);
		System.out.println("Granulação: "+resultInspect.getValues()[2]);
		System.out.println("Esfacelo: "+resultInspect.getValues()[3]);
		System.out.println("Total Pixel quadrado: " + processor.getReferenceArea(wound));
		System.out.println("-------------------------------");
		System.out.println();
		System.out.println("-------- Resultado Humano ----- ");
		System.out.println("Área em cm^2: " + new Double(resultInspect.getValues()[0])/ new Double(processor.getReferenceArea(wound)));
		System.out.println("Área de Necrose: " + new Double(resultInspect.getValues()[1])/new Double(processor.getReferenceArea(wound)));
		System.out.println("Área de Granulação: " + new Double(resultInspect.getValues()[2])/new Double(processor.getReferenceArea(wound)));
		System.out.println("Área de Esfacelo: " + new Double(resultInspect.getValues()[3])/new Double(processor.getReferenceArea(wound)));
		System.out.println("% de Necrose: " + (new Double(resultInspect.getValues()[1])/new Double(resultInspect.getValues()[0]))*100 );
		System.out.println("% de Granulação: " + (new Double(resultInspect.getValues()[2])/new Double(resultInspect.getValues()[0]))*100 );
		System.out.println("% de Esfacelo: " + (new Double(resultInspect.getValues()[3])/new Double(resultInspect.getValues()[0]))*100 );
		System.out.println("---------------------------------");
		
		String oldExt = wound.getExtension();
		
		try {
			 
			String content = "-------------------------------------------------";
			content += "\nFerida: " + wound.getName();
			content +=  "\nArea em cm^2: " + new Double(resultInspect.getValues()[0])/ new Double(processor.getReferenceArea(wound));
			content += "\nArea de Necrose: " + new Double(resultInspect.getValues()[1])/new Double(processor.getReferenceArea(wound));
			content += "\nArea de Granulacao: " + new Double(resultInspect.getValues()[2])/new Double(processor.getReferenceArea(wound));
			content += "\nArea de Esfacelo: " + new Double(resultInspect.getValues()[3])/new Double(processor.getReferenceArea(wound));
			content += "\n% de Necrose: " + (new Double(resultInspect.getValues()[1])/new Double(resultInspect.getValues()[0]))*100 ;
			content += "\n% de Granulacao: " + (new Double(resultInspect.getValues()[2])/new Double(resultInspect.getValues()[0]))*100 ;
			content += "\n% de Esfacelo: " + (new Double(resultInspect.getValues()[3])/new Double(resultInspect.getValues()[0]))*100 ;
			content += "\n-------------------------------------------------";
			
			wound.setExtension(".txt");
			File file = new File(Util.getFullTargetImagePath(wound, "ResultadoTexto"));
 
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
//		//Gerando o gr�fico
		PieChart chart = new PieChart();
		try {
			chart.getPieChart(new BigDecimal((new Double(resultInspect.getValues()[2])/new Double(resultInspect.getValues()[0]))*100).setScale(3, BigDecimal.ROUND_HALF_UP), 
					new BigDecimal((new Double(resultInspect.getValues()[1])/new Double(resultInspect.getValues()[0]))*100).setScale(3, BigDecimal.ROUND_HALF_UP), 
					new BigDecimal((new Double(resultInspect.getValues()[3])/new Double(resultInspect.getValues()[0]))*100).setScale(3, BigDecimal.ROUND_HALF_UP), 
					path);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		spup.setOriginalImage(Util.PATH+image);
		spup.setDelimitedImage(Util.PATH+wound.getName()+"_1_Restult"+oldExt);
		spup.setCutImage(Util.PATH+wound.getName()+"_2_Restult"+oldExt);
		spup.setResultImage(Util.PATH+wound.getName()+"_3_Restult"+oldExt);
		spup.setArea(""+new BigDecimal(new Double(resultInspect.getValues()[0])/ new Double(processor.getReferenceArea(wound))).setScale(3, BigDecimal.ROUND_HALF_UP) + "cm�");
		spup.setNecrose("" + new BigDecimal(new Double(resultInspect.getValues()[1])/new Double(processor.getReferenceArea(wound))).setScale(3, BigDecimal.ROUND_HALF_UP) + "cm� " + " ( "+new BigDecimal((new Double(resultInspect.getValues()[1])/new Double(resultInspect.getValues()[0]))*100).setScale(3, BigDecimal.ROUND_HALF_UP)+"% )");
		spup.setGranulacao("" + new BigDecimal(new Double(resultInspect.getValues()[2])/new Double(processor.getReferenceArea(wound))).setScale(3, BigDecimal.ROUND_HALF_UP) + "cm� " + " ( " + new BigDecimal((new Double(resultInspect.getValues()[2])/new Double(resultInspect.getValues()[0]))*100).setScale(3, BigDecimal.ROUND_HALF_UP) + "% )");
		spup.setEsfacelo(""+ new BigDecimal(new Double(resultInspect.getValues()[3])/new Double(processor.getReferenceArea(wound))).setScale(3, BigDecimal.ROUND_HALF_UP) + "cm� " + " ( " + new BigDecimal((new Double(resultInspect.getValues()[3])/new Double(resultInspect.getValues()[0]))*100).setScale(3, BigDecimal.ROUND_HALF_UP) + "% )");
		spup.setGrafh(path+"pie_Chart3D.jpeg");
		return spup;
		
	}

}
